<?php
    //當未登入或是密碼輸入錯誤時被導回來的時$_GET['login'] 會等於 error
    $login = isset($_GET['login']) ? $_GET['login'] : ''; //if判斷是否存在(條件):成立時顯示 ? 不成立顯示空值
?>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <title>login</title>

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="text-center">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Login</h3>
                    </div>
                    <div class="panel-body">
                        <form action="adlogin.php" method="post">
                            <input type="text" class="form-control" name="account" placeholder="Account" value="abc123" required><br>
                            <input type="password" class="form-control" name="password" placeholder="Password" value="abc123" required><br>
                            <button type="" class="btn btn-warning" id="loginin">登入</button><br><br>
                            <form>
                                <?php
                                    if ($login == 'error') { //當密碼錯誤，或尚未登入時，顯示提示文字
                                        echo "<p class='text-center bg-danger text-white'>帳號或密碼錯誤!</p>";
                                    }
                                ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>